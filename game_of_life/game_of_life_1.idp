vocabulary V {
    type num isa nat
    type Cell constructed from {square(num,num)}
    Xco(Cell):num
    Yco(Cell):num
    Neighbours(Cell,Cell)
    type neighbourAmount = {0..8} isa int
    nbOfLiveNeighbours(Cell):neighbourAmount
    lives(Cell)
}

theory T:V {
    // Define the x-coordinates of a Cell
    {
        Xco(c)=x <- ?y[num]: c=square(x,y).
    }

    // Define the y-coordinates of a Cell
    {
        Yco(c)=y <- ?x[num]: c=square(x,y).
    }

    // Define the neighbours of a Cell
    {
        Neighbours(c1,c2) <- abs(Xco(c1)-Xco(c2))<2
            & abs(Yco(c1)-Yco(c2))<2.
    }

    // Declare how many neighbours of a certain Cell are alive
    !c: nbOfLiveNeighbours(c) = #{ c2[Cell] : lives(c2) & Neighbours(c,c2) }.

    // Cells with exactly three living neighbours must live.
    !c: nbOfLiveNeighbours(c)=3 => lives(c).

    // Each living Cell must have two or three living neighbours.
    !c: lives(c) => 2=< nbOfLiveNeighbours(c) =< 3.
}
empty S0 : V {
  num = { }
}
empty S1 : V {
  num = {1}
}
empty S2 : V {
  num = {1..2}
}
empty S3 : V {
  num = {1..3}
}
