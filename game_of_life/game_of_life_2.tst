/* Congratulations : Tests cover the full program. */
///////// Proposed new test set :
invalid newTest1_1 : V {
  num = { 1..1 }
  Xco = { square(1,1)->1 }
  Yco = { square(1,1)->1 }
  Neighbours = {  }
  nbOfLiveNeighbours = { square(1,1)->3 }
lives = { square(1,1) }
  }

invalid newTest2_4 : V {
  num = { 1..2 }
  Xco = { square(1,1)->1; square(1,2)->1; square(2,1)->2; square(2,2)->2 }
  Yco = { square(1,1)->1; square(1,2)->2; square(2,1)->1; square(2,2)->2 }
  Neighbours = { square(1,1),square(1,2); square(1,1),square(2,1); square(1,1),square(2,2); square(1,2),square(1,1); square(1,2),square(2,1); square(1,2),square(2,2); square(2,1),square(1,1); square(2,1),square(1,2); square(2,1),square(2,2); square(2,2),square(1,1); square(2,2),square(1,2); square(2,2),square(2,1) }
  nbOfLiveNeighbours = { square(1,1)->3; square(1,2)->2; square(2,1)->2; square(2,2)->2 }
lives = { square(1,2); square(2,1); square(2,2) }
  }

invalid newTest2_6 : V {
  num = { 1..2 }
  Xco = { square(1,1)->1; square(1,2)->1; square(2,1)->2; square(2,2)->2 }
  Yco = { square(1,1)->1; square(1,2)->2; square(2,1)->1; square(2,2)->2 }
  Neighbours = { square(1,1),square(1,2); square(1,1),square(2,1); square(1,1),square(2,2); square(1,2),square(1,1); square(1,2),square(2,1); square(1,2),square(2,2); square(2,1),square(1,1); square(2,1),square(1,2); square(2,1),square(2,2); square(2,2),square(1,1); square(2,2),square(1,2); square(2,2),square(2,1) }
  nbOfLiveNeighbours = { square(1,1)->1; square(1,2)->2; square(2,1)->2; square(2,2)->1 }
lives = { square(1,1); square(2,2) }
  }

complete newTest3_7 : V {
  num = { 1..3 }
  Xco = { square(1,1)->1; square(1,2)->1; square(1,3)->1; square(2,1)->2; square(2,2)->2; square(2,3)->2; square(3,1)->3; square(3,2)->3; square(3,3)->3 }
  Yco = { square(1,1)->1; square(1,2)->2; square(1,3)->3; square(2,1)->1; square(2,2)->2; square(2,3)->3; square(3,1)->1; square(3,2)->2; square(3,3)->3 }
  Neighbours = { square(1,1),square(1,2); square(1,1),square(2,1); square(1,1),square(2,2); square(1,2),square(1,1); square(1,2),square(1,3); square(1,2),square(2,1); square(1,2),square(2,2); square(1,2),square(2,3); square(1,3),square(1,2); square(1,3),square(2,2); square(1,3),square(2,3); square(2,1),square(1,1); square(2,1),square(1,2); square(2,1),square(2,2); square(2,1),square(3,1); square(2,1),square(3,2); square(2,2),square(1,1); square(2,2),square(1,2); square(2,2),square(1,3); square(2,2),square(2,1); square(2,2),square(2,3); square(2,2),square(3,1); square(2,2),square(3,2); square(2,2),square(3,3); square(2,3),square(1,2); square(2,3),square(1,3); square(2,3),square(2,2); square(2,3),square(3,2); square(2,3),square(3,3); square(3,1),square(2,1); square(3,1),square(2,2); square(3,1),square(3,2); square(3,2),square(2,1); square(3,2),square(2,2); square(3,2),square(2,3); square(3,2),square(3,1); square(3,2),square(3,3); square(3,3),square(2,2); square(3,3),square(2,3); square(3,3),square(3,2) }
  nbOfLiveNeighbours = { square(1,1)->3; square(1,2)->3; square(1,3)->2; square(2,1)->3; square(2,2)->3; square(2,3)->2; square(3,1)->2; square(3,2)->2; square(3,3)->1 }
lives = { square(1,1); square(1,2); square(2,1); square(2,2) }
  }

vocabulary V {
    type num isa nat
    type Cell constructed from {square(num,num)}
    Xco(Cell):num
    Yco(Cell):num
    Neighbours(Cell,Cell)
    type neighbourAmount = {1..8} isa int
    nbOfLiveNeighbours(Cell):neighbourAmount
    lives(Cell)
}

theory T:V {
    // Define the x-coordinates of a Cell
    {
        ! c[Cell] x[num]:Xco(c)=x <- ?y[num]: c=square(x,y).
    }

    // Define the y-coordinates of a Cell
    {
        ! c[Cell] y[num]:Yco(c)=y <- ?x[num]: c=square(x,y).
    }

    // Define the neighbours of a Cell
    {
        !c1[Cell] c2[Cell]:Neighbours(c1,c2) <- abs(Xco(c1)-Xco(c2))<2
            & abs(Yco(c1)-Yco(c2))<2 & c1~=c2.
    }

    // Declare how many neighbours of a certain Cell are alive
    !c[Cell]: nbOfLiveNeighbours(c) = #{ c2[Cell] : lives(c2) & Neighbours(c,c2) }.

    // Cells with exactly three living neighbours must live.
    !c[Cell]: nbOfLiveNeighbours(c)=3 => lives(c).

    // Each living Cell must have two or three living neighbours.
    !c[Cell]: lives(c) => 2=< nbOfLiveNeighbours(c) =< 3.
}
