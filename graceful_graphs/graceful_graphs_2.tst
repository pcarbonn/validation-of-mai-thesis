/* Congratulations : Tests cover the full program.
Warning : The following predicate(s) could be replaced by true :
  !x[node] y[node]: edge(x,y) & value(x) > value(y) => edge_value(x,y)=value(x)-value(y).
                                ^^^^^^^^^^^^^^^^^^^                                     
  !x[node] y[node]: edge(x,y) & value(x)=< value(y) => edge_value(x,y)=value(x)-value(y).
                                ^^^^^^^^^^^^^^^^^^^                                     
  ! x[node] y[node] x2[node] y2[node]: edge(x,y) & edge(x2,y2) & edge_value(x,y)=edge_value(x2,y2) => x=x2 & y=y2.
                                                                                                      ^^^^   ^^^^
 */
///////// Proposed new test set :
invalid newTest0_1 : V {
  node = { "A" }
  val = { 0..1 }
  value = { "A"->1 }
edge = { "A","A" }
  edge_value = { "A","A"->1 }
  }

invalid newTest0_4 : V {
  node = { "A" }
  val = { 0..1 }
  value = { "A"->1 }
edge = {  }
  edge_value = { "A","A"->1 }
  }

complete newTest1_6 : V {
  node = { "A"; "B" }
  val = { 0..2 }
  value = { "A"->1; "B"->2 }
edge = {  }
  edge_value = { "A","A"->0; "A","B"->0; "B","A"->0; "B","B"->0 }
  }

invalid newTest1_7 : V {
  node = { "A"; "B" }
  val = { 0..2 }
  value = { "A"->1; "B"->2 }
edge = { "B","A" }
  edge_value = { "A","A"->0; "A","B"->0; "B","A"->2; "B","B"->0 }
  }

complete newTest1_9 : V {
  node = { "A"; "B" }
  val = { 0..2 }
  value = { "A"->1; "B"->2 }
edge = { "B","A"; "B","B" }
  edge_value = { "A","A"->0; "A","B"->0; "B","A"->1; "B","B"->0 }
  }

invalid newTest1_12 : V {
  node = { "A"; "B" }
  val = { 0..2 }
  value = { "A"->2; "B"->2 }
edge = {  }
  edge_value = { "A","A"->0; "A","B"->0; "B","A"->0; "B","B"->0 }
  }

invalid newTest1_13 : V {
  node = { "A"; "B" }
  val = { 0..2 }
  value = { "A"->0; "B"->1 }
edge = { "A","A"; "B","A"; "B","B" }
  edge_value = { "A","A"->0; "A","B"->0; "B","A"->1; "B","B"->0 }
  }

vocabulary V{
	type node
	type val isa int
	value(node):val
	edge(node,node)
	edge_value(node,node):val
}

theory T:V{
	!x[node] y[node]: edge(x,y) & value(x) > value(y) => edge_value(x,y)=value(x)-value(y).
	!x[node] y[node]: edge(x,y) & value(x)=< value(y) => edge_value(x,y)=value(x)-value(y).
	!x[node] y[node]: ~edge(x,y) => edge_value(x,y)=0.

	! x[node] y[node] x2[node] y2[node]: edge(x,y) & edge(x2,y2) & edge_value(x,y)=edge_value(x2,y2) => x=x2 & y=y2.

	! n1[node] n2[node]: value(n1)=value(n2)=>n1=n2.
}
