/* Congratulations : Tests cover the full program. */
///////// Proposed new test set :
invalid newTest1_2 : V {
  Color = { "Blue"; "Red" }
  Area = { "BE"; "FR" }
  Border = { "BE","FR" }
  Coloring = { "BE"->"Blue"; "FR"->"Red" }
}

complete newTest1_3 : V {
  Color = { "Blue"; "Red" }
  Area = { "BE"; "FR" }
  Border = { "BE","FR"; "FR","BE" }
  Coloring = { "BE"->"Blue"; "FR"->"Red" }
}

vocabulary V{
	type Color
	type Area
	Border(Area,Area)
	Coloring(Area):Color
}

theory T:V{
	//2 adjacent countries can not have the same color
	!a1[Area] a2[Area]: Border(a1,a2) <= Coloring(a1)~=Coloring(a2).
}

