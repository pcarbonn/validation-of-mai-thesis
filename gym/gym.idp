vocabulary V{
    type SmallInt = {-1000..1000} isa int

    RangordeInschrijving: SmallInt

    type Cursusnaam isa string
    NameOfCourse:Cursusnaam

    GaatNaarSchool // Boolean constant representing whether or not the person goes to school

    type Tijdstip constructed from {september, kerstmis, pasen}
    Inschrijvingsdatum: Tijdstip // Constant representing the unique date at which the person has enrolled

    type Groep isa string
    BepaalGroep(Cursusnaam):Groep

    Supplement:SmallInt
    KortingSchoolgaand:SmallInt
    KortingTweedeInschrijving:SmallInt

    Total:SmallInt
    Basis:SmallInt
}

theory T: V{
    {
        KortingSchoolgaand = 0 <- ~GaatNaarSchool.
        KortingSchoolgaand = -11 <- GaatNaarSchool & Inschrijvingsdatum=september.
        KortingSchoolgaand = -8 <- GaatNaarSchool & Inschrijvingsdatum=kerstmis.
        KortingSchoolgaand = -4 <- GaatNaarSchool & Inschrijvingsdatum=pasen.
    }

    {
        KortingTweedeInschrijving = 0 <- RangordeInschrijving=<1.
        KortingTweedeInschrijving = -18 <- RangordeInschrijving>1 & Inschrijvingsdatum=september.
        KortingTweedeInschrijving = -15 <- RangordeInschrijving>1 & Inschrijvingsdatum=kerstmis.
        KortingTweedeInschrijving = -29 <- RangordeInschrijving>1 & Inschrijvingsdatum=pasen.
    }

    {
        ! c [Cursusnaam]: BepaalGroep(c) = "G2" <- NameOfCourse="aerobics" | NameOfCourse="turners".
        ! c [Cursusnaam]: BepaalGroep(c) = "G1" <- NameOfCourse="minilotjes" | NameOfCourse="tumbling".
    }

    {
        Supplement = 0<- BepaalGroep(NameOfCourse)="G1".
        Supplement = 17<- BepaalGroep(NameOfCourse)="G2" & Inschrijvingsdatum=september.
        Supplement = 11<- BepaalGroep(NameOfCourse)="G2" & Inschrijvingsdatum=kerstmis.
        Supplement = 6<- BepaalGroep(NameOfCourse)="G2" & Inschrijvingsdatum=pasen.
    }

    Total=Basis+KortingTweedeInschrijving+KortingSchoolgaand+Supplement.


}

empty S: V{
    Cursusnaam = {"aerobics";"turners";"minilotjes";"tumbling"}
    Groep = {"G1";"G2"}
    Basis = 100
}
