/* Congratulations : Tests cover the full program. */
///////// Proposed new test set :
complete newTest0_0 : V {
  RangordeInschrijving = 4
  Cursusnaam = { "aerobics"; "minilotjes"; "tumbling"; "turners" }
  NameOfCourse = "turners"
  GaatNaarSchool = false
  Inschrijvingsdatum = pasen
  Groep = { "G1"; "G2" }
  BepaalGroep = { "aerobics"->"G2"; "minilotjes"->"G2"; "tumbling"->"G2"; "turners"->"G2" }
  Supplement = 6
  KortingSchoolgaand = 0
  KortingTweedeInschrijving = -29
  Total = 77
Basis = 100
  }

complete newTest0_1 : V {
  RangordeInschrijving = 5
  Cursusnaam = { "aerobics"; "minilotjes"; "tumbling"; "turners" }
  NameOfCourse = "aerobics"
  GaatNaarSchool = false
  Inschrijvingsdatum = september
  Groep = { "G1"; "G2" }
  BepaalGroep = { "aerobics"->"G2"; "minilotjes"->"G2"; "tumbling"->"G2"; "turners"->"G2" }
  Supplement = 17
  KortingSchoolgaand = 0
  KortingTweedeInschrijving = -18
  Total = 99
Basis = 100
  }

complete newTest0_3 : V {
  RangordeInschrijving = 3
  Cursusnaam = { "aerobics"; "minilotjes"; "tumbling"; "turners" }
  NameOfCourse = "aerobics"
  GaatNaarSchool = false
  Inschrijvingsdatum = kerstmis
  Groep = { "G1"; "G2" }
  BepaalGroep = { "aerobics"->"G2"; "minilotjes"->"G2"; "tumbling"->"G2"; "turners"->"G2" }
  Supplement = 11
  KortingSchoolgaand = 0
  KortingTweedeInschrijving = -15
  Total = 96
Basis = 100
  }

complete newTest0_7 : V {
  RangordeInschrijving = 1
  Cursusnaam = { "aerobics"; "minilotjes"; "tumbling"; "turners" }
  NameOfCourse = "aerobics"
  GaatNaarSchool = true
  Inschrijvingsdatum = september
  Groep = { "G1"; "G2" }
  BepaalGroep = { "aerobics"->"G2"; "minilotjes"->"G2"; "tumbling"->"G2"; "turners"->"G2" }
  Supplement = 17
  KortingSchoolgaand = -11
  KortingTweedeInschrijving = 0
  Total = 106
Basis = 100
  }

complete newTest0_8 : V {
  RangordeInschrijving = 1
  Cursusnaam = { "aerobics"; "minilotjes"; "tumbling"; "turners" }
  NameOfCourse = "aerobics"
  GaatNaarSchool = true
  Inschrijvingsdatum = kerstmis
  Groep = { "G1"; "G2" }
  BepaalGroep = { "aerobics"->"G2"; "minilotjes"->"G2"; "tumbling"->"G2"; "turners"->"G2" }
  Supplement = 11
  KortingSchoolgaand = -8
  KortingTweedeInschrijving = 0
  Total = 103
Basis = 100
  }

complete newTest0_9 : V {
  RangordeInschrijving = 1
  Cursusnaam = { "aerobics"; "minilotjes"; "tumbling"; "turners" }
  NameOfCourse = "aerobics"
  GaatNaarSchool = true
  Inschrijvingsdatum = pasen
  Groep = { "G1"; "G2" }
  BepaalGroep = { "aerobics"->"G2"; "minilotjes"->"G2"; "tumbling"->"G2"; "turners"->"G2" }
  Supplement = 6
  KortingSchoolgaand = -4
  KortingTweedeInschrijving = 0
  Total = 102
Basis = 100
  }

complete newTest0_13 : V {
  RangordeInschrijving = 2
  Cursusnaam = { "aerobics"; "minilotjes"; "tumbling"; "turners" }
  NameOfCourse = "tumbling"
  GaatNaarSchool = true
  Inschrijvingsdatum = september
  Groep = { "G1"; "G2" }
  BepaalGroep = { "aerobics"->"G1"; "minilotjes"->"G1"; "tumbling"->"G1"; "turners"->"G1" }
  Supplement = 0
  KortingSchoolgaand = -11
  KortingTweedeInschrijving = -18
  Total = 71
Basis = 100
  }

complete newTest0_14 : V {
  RangordeInschrijving = 2
  Cursusnaam = { "aerobics"; "minilotjes"; "tumbling"; "turners" }
  NameOfCourse = "tumbling"
  GaatNaarSchool = true
  Inschrijvingsdatum = kerstmis
  Groep = { "G1"; "G2" }
  BepaalGroep = { "aerobics"->"G1"; "minilotjes"->"G1"; "tumbling"->"G1"; "turners"->"G1" }
  Supplement = 0
  KortingSchoolgaand = -8
  KortingTweedeInschrijving = -15
  Total = 77
Basis = 100
  }

invalid newTest0_17 : V {
  RangordeInschrijving = 3
  Cursusnaam = { "aerobics"; "minilotjes"; "tumbling"; "turners" }
  NameOfCourse = "aerobics"
  GaatNaarSchool = true
  Inschrijvingsdatum = pasen
  Groep = { "G1"; "G2" }
  BepaalGroep = { "aerobics"->"G2"; "minilotjes"->"G2"; "tumbling"->"G2"; "turners"->"G2" }
  Supplement = 6
  KortingSchoolgaand = -4
  KortingTweedeInschrijving = -29
  Total = 2
Basis = 100
  }

complete newTest0_18 : V {
  RangordeInschrijving = 2
  Cursusnaam = { "aerobics"; "minilotjes"; "tumbling"; "turners" }
  NameOfCourse = "minilotjes"
  GaatNaarSchool = true
  Inschrijvingsdatum = pasen
  Groep = { "G1"; "G2" }
  BepaalGroep = { "aerobics"->"G1"; "minilotjes"->"G1"; "tumbling"->"G1"; "turners"->"G1" }
  Supplement = 0
  KortingSchoolgaand = -4
  KortingTweedeInschrijving = -29
  Total = 67
Basis = 100
  }

vocabulary V{
    type SmallInt = {-1000..1000} isa int

    RangordeInschrijving: SmallInt

    type Cursusnaam isa string
    NameOfCourse:Cursusnaam

    GaatNaarSchool // Boolean constant representing whether or not the person goes to school

    type Tijdstip constructed from {september, kerstmis, pasen}
    Inschrijvingsdatum: Tijdstip // Constant representing the unique date at which the person has enrolled

    type Groep isa string
    BepaalGroep(Cursusnaam):Groep

    Supplement:SmallInt
    KortingSchoolgaand:SmallInt
    KortingTweedeInschrijving:SmallInt

    Total:SmallInt
    Basis:SmallInt
}

theory T: V{
    {
        KortingSchoolgaand = 0 <- ~GaatNaarSchool.
        KortingSchoolgaand = -11 <- GaatNaarSchool & Inschrijvingsdatum=september.
        KortingSchoolgaand = -8 <- GaatNaarSchool & Inschrijvingsdatum=kerstmis.
        KortingSchoolgaand = -4 <- GaatNaarSchool & Inschrijvingsdatum=pasen.
    }

    {
        KortingTweedeInschrijving = 0 <- RangordeInschrijving=<1.
        KortingTweedeInschrijving = -18 <- RangordeInschrijving>1 & Inschrijvingsdatum=september.
        KortingTweedeInschrijving = -15 <- RangordeInschrijving>1 & Inschrijvingsdatum=kerstmis.
        KortingTweedeInschrijving = -29 <- RangordeInschrijving>1 & Inschrijvingsdatum=pasen.
    }

    {
        ! c [Cursusnaam]: BepaalGroep(c) = "G2" <- NameOfCourse="aerobics" | NameOfCourse="turners".
        ! c [Cursusnaam]: BepaalGroep(c) = "G1" <- NameOfCourse="minilotjes" | NameOfCourse="tumbling".
    }

    {
        Supplement = 0<- BepaalGroep(NameOfCourse)="G1".
        Supplement = 17<- BepaalGroep(NameOfCourse)="G2" & Inschrijvingsdatum=september.
        Supplement = 11<- BepaalGroep(NameOfCourse)="G2" & Inschrijvingsdatum=kerstmis.
        Supplement = 6<- BepaalGroep(NameOfCourse)="G2" & Inschrijvingsdatum=pasen.
    }

    Total=Basis+KortingTweedeInschrijving+KortingSchoolgaand+Supplement.


}

