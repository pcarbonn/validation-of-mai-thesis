/* Congratulations : Tests cover the full program. */
///////// Proposed new test set :
complete newTest0_0 : V {
  city = { "BRU" }
  Road = {  }
  Path = {  }
  Start = "BRU"
End = "BRU"
  Reaches = {  }
  }

invalid newTest0_4 : V {
  city = { "BRU" }
  Road = {  }
  Path = { "BRU","BRU" }
  Start = "BRU"
End = "BRU"
  Reaches = { "BRU","BRU" }
  }

complete newTest1_5 : V {
  city = { "BRU"; "LEU" }
  Road = { "BRU","BRU"; "BRU","LEU"; "LEU","BRU"; "LEU","LEU" }
  Path = { "BRU","BRU"; "LEU","LEU" }
  Start = "BRU"
End = "BRU"
  Reaches = { "BRU","BRU"; "LEU","LEU" }
  }

complete newTest1_6 : V {
  city = { "BRU"; "LEU" }
  Road = { "BRU","BRU"; "BRU","LEU"; "LEU","BRU"; "LEU","LEU" }
  Path = { "BRU","LEU" }
  Start = "BRU"
End = "BRU"
  Reaches = { "BRU","BRU"; "BRU","LEU"; "LEU","BRU"; "LEU","LEU" }
  }

vocabulary V{
    type city
    Road(city,city)
    Path(city,city)
    Start:city
    End:city
    Reaches(city,city)
}

theory T:V{
    !x[city] y[city]:Path(x,y)=>Road(x,y).
    {
        !x y:Reaches(x,y) <- Path(x,y) | Path(y,x).
        !x y:Reaches(x,y) <- ?z[city]: Reaches(x,z) & Reaches(z,y).
    }
}
