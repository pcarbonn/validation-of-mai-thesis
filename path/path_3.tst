/* Congratulations : Tests cover the full program. */
///////// Proposed new test set :
invalid newTest0_2 : V {
  city = { "BRU" }
  Road = {  }
  Path = {  }
  Start = "BRU"
End = "BRU"
  Reaches = {  }
  }

complete newTest1_3 : V {
  city = { "BRU"; "LEU" }
  Road = {  }
  Path = { "BRU","BRU"; "LEU","LEU" }
  Start = "LEU"
End = "LEU"
  Reaches = { "BRU","BRU"; "LEU","LEU" }
  }

complete newTest1_4 : V {
  city = { "BRU"; "LEU" }
  Road = {  }
  Path = { "BRU","LEU" }
  Start = "LEU"
End = "LEU"
  Reaches = { "BRU","BRU"; "BRU","LEU"; "LEU","BRU"; "LEU","LEU" }
  }

vocabulary V{
    type city
    Road(city,city)
    Path(city,city)
    Start:city
    End:city
    Reaches(city,city)
}

theory T:V{
    Reaches(Start,End).
    {
        !x[city] y[city]:Reaches(x,y) <- Path(x,y) | Path(y,x).
        !x[city] y[city]:Reaches(x,y) <- ?z[city]: Reaches(x,z) & Reaches(z,y).
    }
}
