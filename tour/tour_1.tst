/* Congratulations : Tests cover the full program. */
///////// Proposed new test set :
invalid newTest0_1 : V {
  City = { 1..1 }
  InPath = { 1,1 }
}

complete newTest1_2 : V {
  City = { 1..2 }
  InPath = { 1,2; 2,1 }
}

invalid newTest2_3 : V {
  City = { 1..3 }
  InPath = { 2,1; 2,3; 3,2 }
}

invalid newTest2_4 : V {
  City = { 1..3 }
  InPath = { 1,2; 2,3; 3,2 }
}

vocabulary V{
    type City
    InPath(City,City)
}

theory T:V{
    !x[City]: ?1y[City]: InPath(x,y).
    !x[City]: ?1y[City]: InPath(y,x).
    !x[City]: ~InPath(x,x).
}
